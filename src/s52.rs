use crate::alloc::{Allocator, Global, Layout};
type AvxBase = u64;
type DoubleAvxBase = u128;
impl_avx_base! {ModExt, 52, ModBase, AvxBase, DoubleAvxBase}

use crate::vec_mul::*;

#[doc(cfg(target_feature = "avx512ifma"))]
impl_avx_mul! {
    Mul2, ModBase, 2, impl_avx_mul_func_inner, (_mm_,m128,cmpneq_epu64_mask,mask_add_epi64,cmpge_epu64_mask,mask_sub_epi64;madd52lo_epu64,madd52hi_epu64,mullo,mulhi), (reslo, reshi),
    (
        mul,crate::simd::m128,doc(cfg(target_feature = "avx512ifma")),doc="Multply 2 52-bit integer together and then store the results to reslo and reshi.";
        mul_assign,core::mem::transmute_copy,doc(cfg(target_feature = "avx512ifma")),doc="Multply 2 52-bit integer together, `add_assign` the results to reslo and reshi."
    )
}
#[doc(cfg(target_feature = "avx512ifma"))]
impl_avx_mul! {
    Mul4, ModBase, 4, impl_avx_mul_func_inner, (_mm256_,m256,cmpneq_epu64_mask,mask_add_epi64,cmpge_epu64_mask,mask_sub_epi64;madd52lo_epu64,madd52hi_epu64,mullo,mulhi), (reslo, reshi),
    (
        mul,crate::simd::m256,doc(cfg(target_feature = "avx512ifma")),doc="Multply 4 52-bit integer together and then store the results to reslo and reshi.";
        mul_assign,core::mem::transmute_copy,doc(cfg(target_feature = "avx512ifma")),doc="Multply 4 52-bit integer together, `add_assign` the results to reslo and reshi."
    )
}
#[doc(cfg(target_feature = "avx512ifma"))]
impl_avx_mul! {
    Mul8, ModBase, 8, impl_avx_mul_func_inner, (_mm512_,m512,cmpneq_epu64_mask,mask_add_epi64,cmpge_epu64_mask,mask_sub_epi64;madd52lo_epu64,madd52hi_epu64,mullo,mulhi), (reslo, reshi),
    (
        mul,crate::simd::m512,doc(cfg(target_feature = "avx512ifma")),doc="Multply 8 52-bit integer together and then store the results to reslo and reshi.";
        mul_assign,core::mem::transmute_copy,doc(cfg(target_feature = "avx512ifma")),doc="Multply 8 52-bit integer together, `add_assign` the results to reslo and reshi."
    )
}
impl Mul16 for ModBase {}
#[cfg(not(any(doc, target_feature = "avx512ifma")))]
impl Mul2 for ModBase {}
#[cfg(not(any(doc, target_feature = "avx512ifma")))]
impl Mul4 for ModBase {}
#[cfg(not(any(doc, target_feature = "avx512ifma")))]
impl Mul8 for ModBase {}
impl Mul1 for ModBase {
    fn mul(a: &[Self; 1], b: &[Self; 1], reslo: &mut [Self; 1], reshi: &mut [Self; 1]) {
        let tmp = a[0].as_double() * b[0].as_double();
        reslo[0] = ModBase::from_double(tmp & (ModBase::RD - 1));
        reshi[0] = ModBase::from_double(tmp >> ModBase::SHIFT);
    }
    fn mul_assign(a: &[Self; 1], b: &[Self; 1], reslo: &mut [Self; 1], reshi: &mut [Self; 1]) {
        let tmp = a[0].as_double() * b[0].as_double();
        reslo[0] = ModBase::from_double(reslo[0].as_double() + (tmp & (ModBase::RD - 1)));
        reshi[0] = ModBase::from_double(reslo[0].as_double() + (tmp >> ModBase::SHIFT));
    }
    unsafe fn mullo(a: &[Self; 1], b: &[Self; 1], reslo: [Self; 1]) -> [Self; 1] {
        let tmp = a[0].as_double() * b[0].as_double();
        [ModBase(
            ModBase::from_double(tmp & (ModBase::RD - 1)).0 + reslo[0].0,
        )]
    }
    unsafe fn mulhi(a: &[Self; 1], b: &[Self; 1], reshi: [Self; 1]) -> [Self; 1] {
        let tmp = a[0].as_double() * b[0].as_double();
        [ModBase(
            ModBase::from_double(tmp >> ModBase::SHIFT).0 + reshi[0].0,
        )]
    }
}

pub struct MArr(pub Box<[ModBase]>);
impl MArr {
    /// Fit the specific requirement of align.
    ///
    /// Store 512-bits of integer data from a into memory. mem_addr must be aligned on a 64-byte boundary or a general-protection exception may be generated.
    const ALIGN: usize = 64;
    pub fn new(size: impl Into<u64>) -> Self {
        let size = size.into() as usize * core::mem::size_of::<ModBase>();
        let layout = unsafe {
            // SAFETY: align is a power of 2, fit the 64-bit boundary.
            Layout::from_size_align_unchecked(size, Self::ALIGN)
        };
        let ptr = core::ptr::NonNull::<[ModBase]>::slice_from_raw_parts(
            Global.allocate_zeroed(layout).unwrap().cast(),
            size / core::mem::size_of::<ModBase>(),
        );
        // SAFETY: see https://doc.rust-lang.org/alloc/boxed/index.html#memory-layout
        // It is valid to convert both ways between a Box and a raw pointer allocated
        // with the Global allocator, given that the Layout used with the allocator is
        // correct for the type. More precisely, a value: *mut T that has been allocated
        // with the Global allocator with Layout::for_value(&*value) may be converted
        // into a box using Box::<T>::from_raw(value)
        MArr(unsafe { Box::from_raw(ptr.as_ptr()) })
    }
}
