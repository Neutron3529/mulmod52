pub trait SugarMul: Sized {
    fn mul(&self, other: &Self, lo: &mut Self, hi: &mut Self);
    fn mul_assign(&self, other: &Self, lo: &mut Self, hi: &mut Self);
    unsafe fn mulhi(&self, other: &Self, res: Self) -> Self;
    unsafe fn mullo(&self, other: &Self, res: Self) -> Self;
    fn neq0p1(&self, other: &mut Self);
    fn sub_ge(&mut self, other: Self);
}
use crate::vec_mul::*;
sugar_mul!(Mul1, 1, Mul2, 2, Mul4, 4, Mul8, 8, Mul16, 16);
