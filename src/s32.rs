use crate::alloc::{Allocator, Global, Layout};
type AvxBase = u32;
type DoubleAvxBase = u64;
impl_avx_base! {ModExt, 32, ModBase, AvxBase, DoubleAvxBase}

use crate::vec_mul::*;
impl Mul1 for ModBase {
    fn mul(a: &[Self; 1], b: &[Self; 1], reslo: &mut [Self; 1], reshi: &mut [Self; 1]) {
        let tmp = a[0].as_double() * b[0].as_double();
        reslo[0] = ModBase::from_double(tmp & (ModBase::RD - 1));
        reshi[0] = ModBase::from_double(tmp >> ModBase::SHIFT);
    }
    fn mul_assign(a: &[Self; 1], b: &[Self; 1], reslo: &mut [Self; 1], reshi: &mut [Self; 1]) {
        let tmp = a[0].as_double() * b[0].as_double();
        reslo[0] = ModBase::from_double(reslo[0].as_double() + (tmp & (ModBase::RD - 1)));
        reshi[0] = ModBase::from_double(reslo[0].as_double() + (tmp >> ModBase::SHIFT));
    }
    unsafe fn mullo(a: &[Self; 1], b: &[Self; 1], reslo: [Self; 1]) -> [Self; 1] {
        let tmp = a[0].as_double() * b[0].as_double();
        [ModBase(
            ModBase::from_double(tmp & (ModBase::RD - 1)).0 + reslo[0].0,
        )]
    }
    unsafe fn mulhi(a: &[Self; 1], b: &[Self; 1], reshi: [Self; 1]) -> [Self; 1] {
        let tmp = a[0].as_double() * b[0].as_double();
        [ModBase(
            ModBase::from_double(tmp >> ModBase::SHIFT).0 + reshi[0].0,
        )]
    }
}
impl Mul2 for ModBase {}
#[cfg(not(any(target_feature = "avx2", doc)))]
impl Mul4 for ModBase {}
#[cfg(not(any(target_feature = "avx2", doc)))]
impl Mul8 for ModBase {}
#[cfg(not(any(target_feature = "avx512f", doc)))]
impl Mul16 for ModBase {}
// TODO: add support of these functions
// _mm512_mulhi_epu32
// _mm512_mullo_epi32
pub struct MArr(pub Box<[ModBase]>);
impl MArr {
    /// Fit the specific requirement of align.
    ///
    /// Store 512-bits of integer data from a into memory. mem_addr must be aligned on a 64-byte boundary or a general-protection exception may be generated.
    const ALIGN: usize = 64;
    pub fn new(size: impl Into<u64>) -> Self {
        let size = size.into() as usize * core::mem::size_of::<ModBase>();
        let layout = unsafe {
            // SAFETY: align is a power of 2, fit the 64-bit boundary.
            Layout::from_size_align_unchecked(size, Self::ALIGN)
        };
        let ptr = core::ptr::NonNull::<[ModBase]>::slice_from_raw_parts(
            Global.allocate_zeroed(layout).unwrap().cast(),
            size / core::mem::size_of::<ModBase>(),
        );
        // SAFETY: see https://doc.rust-lang.org/alloc/boxed/index.html#memory-layout
        // It is valid to convert both ways between a Box and a raw pointer allocated
        // with the Global allocator, given that the Layout used with the allocator is
        // correct for the type. More precisely, a value: *mut T that has been allocated
        // with the Global allocator with Layout::for_value(&*value) may be converted
        // into a box using Box::<T>::from_raw(value)
        MArr(unsafe { Box::from_raw(ptr.as_ptr()) })
    }
}
