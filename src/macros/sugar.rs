macro_rules! sugar_mul {
    ($($Mul:ident,$len:literal),*) => {
        $(impl<T: $Mul> SugarMul for [T; $len] {
            fn mul(&self, other: &Self, lo: &mut Self, hi: &mut Self) {
                <T as $Mul>::mul(self, other, lo, hi)
            }
            fn mul_assign(&self, other: &Self, lo: &mut Self, hi: &mut Self) {
                <T as $Mul>::mul_assign(self, other, lo, hi)
            }
            /// SAFETY: res should be aligned properly
            unsafe fn mulhi(&self, other: &Self, res:Self) ->Self {
                <T as $Mul>::mulhi(self,other,res)
            }
            /// SAFETY: res should be aligned properly
            unsafe fn mullo(&self, other: &Self, res:Self) ->Self {
                <T as $Mul>::mullo(self,other,res)
            }
            fn neq0p1(&self, other:&mut Self){
                <T as $Mul>::neq0p1(self,other)
            }
            fn sub_ge(&mut self, other:Self){
                <T as $Mul>::sub_ge(self,other)
            }
        })*
    };
}
