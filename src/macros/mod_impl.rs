macro_rules! mul_impl {
    ($(($trait:ident,$father:ident,$depth:expr,$($meta:meta),*)),+)=>{
        $(pub trait $trait : $father + Sized + Copy + ModOps {
            $(#[$meta])*
            fn mul_assign(a: &[Self;{$depth*2}], b: &[Self;{$depth*2}], reslo: &mut [Self;{$depth*2}], reshi: &mut [Self;{$depth*2}]){
                let (a, b, l, h) = unsafe {
                    let a = a.as_chunks_unchecked::<$depth>();
                    let b = b.as_chunks_unchecked::<$depth>();
                    let l = reslo.as_chunks_unchecked_mut::<$depth>();
                    let h = reshi.as_chunks_unchecked_mut::<$depth>();
                    (a, b, l, h)
                };
                <Self as $father>::mul_assign(&a[0], &b[0], &mut l[0], &mut h[0]);
                <Self as $father>::mul_assign(&a[1], &b[1], &mut l[1], &mut h[1]);
            }
            $(#[$meta])*
            fn mul(a: &[Self;{$depth*2}], b: &[Self;{$depth*2}], reslo: &mut [Self;{$depth*2}], reshi: &mut [Self;{$depth*2}]){
                let (a, b, l, h) = unsafe {
                    let a = a.as_chunks_unchecked::<$depth>();
                    let b = b.as_chunks_unchecked::<$depth>();
                    let l = reslo.as_chunks_unchecked_mut::<$depth>();
                    let h = reshi.as_chunks_unchecked_mut::<$depth>();
                    (a, b, l, h)
                };
                <Self as $father>::mul(&a[0], &b[0], &mut l[0], &mut h[0]);
                <Self as $father>::mul(&a[1], &b[1], &mut l[1], &mut h[1]);
            }
            /// low level function, may contains extra cost, do not use unless you know what you are doing.
            ///
            /// SAFETY: reslo must be aligned properly.
            $(#[$meta])*
            unsafe fn mullo(a: &[Self;{$depth*2}], b: &[Self;{$depth*2}], mut reslo: [Self;{$depth*2}])->[Self;{$depth*2}]{
                let (a, b, l) = unsafe {
                    let a = a.as_chunks_unchecked::<$depth>();
                    let b = b.as_chunks_unchecked::<$depth>();
                    let l = reslo.as_chunks_unchecked_mut::<$depth>();
                    (a, b, l)
                };
                l[0]=<Self as $father>::mullo(&a[0], &b[0], l[0]);
                l[1]=<Self as $father>::mullo(&a[1], &b[1], l[1]);
                reslo
            }
            /// low level function, may contains extra cost, do not use unless you know what you are doing.
            ///
            /// SAFETY: reshi must be aligned properly.
            $(#[$meta])*
            unsafe fn mulhi(a: &[Self;{$depth*2}], b: &[Self;{$depth*2}], mut reshi: [Self;{$depth*2}])->[Self;{$depth*2}]{
                let (a, b, h) = unsafe {
                    let a = a.as_chunks_unchecked::<$depth>();
                    let b = b.as_chunks_unchecked::<$depth>();
                    let h = reshi.as_chunks_unchecked_mut::<$depth>();
                    (a, b, h)
                };
                h[0]=<Self as $father>::mulhi(&a[0], &b[0], h[0]);
                h[1]=<Self as $father>::mulhi(&a[1], &b[1], h[1]);
                reshi
            }
            fn neq0p1(a: &[Self; {$depth*2}], target: &mut [Self; {$depth*2}]){
                target.iter_mut().zip(a.iter()).for_each(|(x,a)|x.add(a.sgn()))
            }
            fn sub_ge(target: &mut [Self; {$depth*2}], base: [Self; {$depth*2}]){
                target.iter_mut().zip(base.iter()).for_each(|(x,&a)|x.sub_ge(a))
            }
        })+
    };
}
