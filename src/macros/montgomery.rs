macro_rules! mmul{
    // s52, ModBase, ModExt, MMul1 ,Mul1 ,1, _mm_,m128,cmpneq_epi64_mask,mask_add_epi64
    ($($ModBase:ty, $ModExt:ty, $MMul:ident, $Mul:ident, $size:expr, $broadcast:ident,$base:tt,$pinv:tt,$rsq:tt);*)=>{
        $(pub trait $MMul : $Mul {
            #[inline(always)]
            fn mmul(a: &[Self;$size], b: &[Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt);
            #[inline(always)]
            fn mmul_inplace(a: &mut [Self;$size], b: &[Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt){
                Self::mmul(a,b,buffer1,result,p);
                *a=*result;
            }
            /// Perform a montgomery transform
            #[inline(always)]
            fn mt(a: &mut [Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt);
            /// Recover numbers from a montgomery transform
            #[inline(always)]
            fn imt(a: &mut [Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt);
        }
        impl $MMul for $ModBase {
            #[inline(always)]
            fn mmul(a: &[Self;$size], b: &[Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt){
                use crate::{simd::*,sugar::SugarMul};
                a.mul(b,buffer1,result); // t=a*b
                *buffer1=unsafe {buffer1.mullo(core::mem::transmute(&p.avx.$pinv.$broadcast),core::mem::transmute(AVX_ZERO.$broadcast))}; // buffer1 = t%r * pinv%r. latency=4
                a.neq0p1(result);
                *result=unsafe {buffer1.mulhi(core::mem::transmute(&p.avx.$base.$broadcast),*result)}; // (t/r + a!=0) + buffer1*p/r =(t+buffer1*p)/r
                result.sub_ge(unsafe { core::mem::transmute(p.avx.$base.$broadcast) })
            }
            /// Perform a montgomery transform
            #[inline(always)]
            fn mt(a: &mut [Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt){
                unsafe {$MMul::mmul_inplace(a,&core::mem::transmute(p.avx.$rsq.$broadcast),buffer1,result,p)}
            }
            /// Recover numbers from a montgomery transform
            #[inline(always)]
            fn imt(a: &mut [Self;$size], buffer1: &mut [Self;$size], result: &mut [Self;$size], p: $ModExt){
                unsafe {$MMul::mmul_inplace(a,&core::mem::transmute(<$ModBase>::AVX_ONE.$broadcast),buffer1,result,p)}
            }
        }
        )*
    }
}
