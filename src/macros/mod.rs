#[macro_use]
pub(crate) mod avx_common;
#[macro_use]
pub(crate) mod mod_impl;
#[macro_use]
pub(crate) mod sugar;
#[macro_use]
pub(crate) mod montgomery;

macro_rules! mark_as_avx_int {
    ($($ty:ident),*) => {
        $(impl const AvxInt for $ty {
            unsafe fn broadcast(self) -> __m512i {
                core::mem::transmute([self; 64 / core::mem::size_of::<Self>()])
            }
        })*
    };
}
