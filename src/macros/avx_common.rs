macro_rules! impl_avx_base {
    {$i:ident, $R:literal, $modbase:ident, $base:ty, $double:ty} => {
        #[repr(transparent)]
        #[derive(Debug,Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
        pub struct $modbase(pub $base);
        impl $modbase {
            pub const AVX_ONE:crate::simd::Avx512=crate::simd::Avx512::new(1 as $base);
            pub const SHIFT:u32=$R;
            pub const RD:$double=(1 as $double).wrapping_shl(Self::SHIFT); // sometimes it could be 0, but that's fine.
            pub const R:$base=Self::RD as $base;
            #[inline(always)]
            pub const fn new(i: $base) -> Self {
                Self(i)
            }
            #[inline(always)]
            pub const fn as_double(&self)->$double{
                self.0 as $double
            }
            #[inline(always)]
            pub const fn from_double(i:$double)->Self{
                Self(i as $base)
            }
        }
        pub(crate) const fn mod_pow(n: $base, mut m: $base, p: $double) -> $base {
            let [mut curr, mut res] = [n as $double, 1];
            while m != 0 {
                if m & 1 == 1 {
                    res = (res * curr) % p
                }
                curr = (curr * curr) % p;
                m >>= 1;
            }
            res as $base
        }
        use crate::simd::Avx512;
        #[derive(Clone, Copy)]
        pub struct $i {
            pub base: $modbase,
            // r=2.pow(52).
            pinv: $base,
            rsq: $base,
            pub(crate) avx:(Avx512,Avx512,Avx512),
        }
        impl $i {
            // pub const R:$base=(1 as $base).wrapping_shl($R); // sometimes it could be 0, but that's fine.
            pub const SHIFT:u32=ModBase::SHIFT;
            pub const RD:$double=ModBase::RD; // sometimes it could be 0, but that's fine.
            const R:$base=ModBase::R;
            /// create a new object
            pub const fn new(base: $base) -> Self {
                let base = <$modbase>::new(base);
                // SAFETY: This is always safe to construct pinv and rsq.
                let pinv=unsafe {Self::calc_pinv(base)};
                let rsq=unsafe {Self::calc_rsq(base)};
                Self {
                    base,
                    pinv,
                    rsq,
                    avx:(Avx512::new(base.0),Avx512::new(pinv),Avx512::new(rsq))
                }
            }
            pub fn rebase(&mut self, base: $base) {
                self.base = <$modbase>::new(base);
                // SAFETY:
                // calling pinv and rsq while changing the base is needed.
                self.pinv = unsafe { Self::calc_pinv(self.base) };
                self.rsq = unsafe { Self::calc_rsq(self.base) };
                self.avx=(Avx512::new(self.base.0),Avx512::new(self.pinv),Avx512::new(self.rsq))
            }
            /// This function is VERY UNSAFE
            /// YOU SHOULD NOT CALL THIS FUNCTION unless you have a faster approach to compute pinv and rsq
            #[inline(always)]
            pub unsafe fn force_rebase(&mut self, base: $modbase) {
                self.base=base
            }
            /// This function is safe, but calling this function is highly unsafe for users.
            /// Proper results are already cached and could obtained directly by calling the function without `calc_` prefix.
            /// The only situation you use such function is that, you modify the cached result with some very dangerous function.
            /// Otherwise, such function should not called manually.
            #[inline(always)]
            pub const unsafe fn calc_pinv(base: $modbase) -> $base {
                Self::R.wrapping_sub(mod_pow(base.0, Self::R.wrapping_sub(1), Self::RD))
            }
            /// This function is safe, but calling this function is highly unsafe for users.
            /// Proper results are already cached and could obtained directly by calling the function without `calc_` prefix.
            /// The only situation you use such function is that, you modify the cached result with some very dangerous function.
            /// Otherwise, such function should not called manually.
            #[inline(always)]
            pub const unsafe fn calc_rsq(base: $modbase) -> $base {
                let a=Self::RD%base.0 as $double;
                (a*a%base.0 as $double) as $base
            }
            /// base, obtain the modbase.
            /// since you are not expected to modify base directly, only getter is provided with low cost.
            /// calling setter (`self.rebase(base)`) would re-calculate `rsq` and `pinv` automatically.
            /// and you must manually modify `pinv` and `rsq` after a manually `force_rebase` is called.
            #[inline(always)]
            pub const fn base(&self) -> $base {
                self.base.0
            }
            /// since you are not expected to modify either pinv or rsq, only getter is provided.
            #[inline(always)]
            pub const fn pinv(&self) -> $base {
                self.pinv
            }
            /// since you are not expected to modify either pinv or rsq, only getter is provided.
            #[inline(always)]
            pub const fn rsq(&self) -> $base {
                self.rsq
            }
        }

        impl From<$modbase> for $i {
            fn from(base: $modbase) -> Self {
                Self::new(base.0)
            }
        }
        impl PartialEq for $i {
            fn eq(&self, other: &Self) -> bool {
                self.base.0 == other.base.0
            }
        }
        impl PartialOrd for $i {
            fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
                Some(self.base.0.cmp(&other.base.0))
            }
        }
        impl ModOps for $modbase {
            type ModBase=$base;
            #[inline(always)]
            fn add(&mut self,add:Self::ModBase){
                self.0+=add
            }
            #[inline(always)]
            fn sgn(&self)->Self::ModBase{
                (self.0!=0) as $base
            }
            #[inline(always)]
            fn sub_ge(&mut self,other:Self){
                if self.0>=other.0 {
                    self.0-=other.0
                }
            }
        }
    };
}
macro_rules! impl_avx_mul_func_inner {
    ($($to:ident, $acc:path, $begin:ident,$calc:ident,$a:ident,$b:ident);+) => {{
        use crate::simd::*;
        $(*$to =
            core::mem::transmute(
                concat_idents!($begin, $calc)(
                    $acc($to),
                    core::mem::transmute_copy($a),
                    core::mem::transmute_copy($b),
                )
            )
        );+
    }};
}
macro_rules! impl_avx_mul_func {
    ($mul:ident, $acc:path, $count:literal, $begin:ident, $impl_avx_mul_func_inner:ident, ($reslo:ident, $reshi:ident), ($($res:ident, $calc:ident);+), $($meta:meta),*) => {
        $(#[$meta])*
        fn $mul(a: &[Self; $count], b: &[Self; $count], $reslo: &mut [Self; $count], $reshi: &mut [Self; $count]){
            unsafe {
                $impl_avx_mul_func_inner!($($res,$acc,$begin,$calc,a,b);+);
            }
        }
    };
}
macro_rules! impl_avx_mul_lohi {
    ($mul:ident, $count:literal, $begin:ident,$calc:ident) => {
        /// low level function, may contains extra cost, do not use unless you know what you are doing.
        ///
        /// SAFETY: res must be aligned properly.
        #[inline(always)]
        unsafe fn $mul(
            a: &[Self; $count],
            b: &[Self; $count],
            res: [Self; $count],
        ) -> [Self; $count] {
            use crate::simd::*;
            core::mem::transmute(concat_idents!($begin, $calc)(
                core::mem::transmute(res),
                core::mem::transmute_copy(a),
                core::mem::transmute_copy(b),
            ))
        }
    };
}
macro_rules! impl_avx_mul {
    {$impl_trait:ty, $for_struct:ty, $count:literal, $impl_avx_mul_func_inner:ident,
        ($begin:ident,$broadcast:ident,$neq:ident,$maskadd:ident,$ge:ident,$masksub:ident;$lo:ident,$hi:ident,$mullo:ident,$mulhi:ident),
        ($reslo:ident, $reshi:ident),
        (
            $mul0:ident, $acc0:path, $($meta0:meta),*;
            $mul:ident, $acc:path, $($meta:meta),*
        ) } => {
        impl $impl_trait for $for_struct {
            impl_avx_mul_lohi!($mullo,$count,$begin,$lo);
            impl_avx_mul_lohi!($mulhi,$count,$begin,$hi);
            impl_avx_mul_func!($mul0,$acc0,$count,$begin,$impl_avx_mul_func_inner,($reslo,$reshi),($reslo,$lo;$reshi,$hi),$($meta0),*);
            impl_avx_mul_func!($mul ,$acc ,$count,$begin,$impl_avx_mul_func_inner,($reslo,$reshi),($reslo,$lo;$reshi,$hi),$($meta) ,*);
            #[inline(always)]
            fn neq0p1(a: &[Self; $count], result: &mut [Self; $count]) {
                use crate::simd::*;
                let mask=unsafe {concat_idents!($begin,$neq)(core::mem::transmute_copy(a),AVX_ZERO.$broadcast)}; // mask=a!=0, fast. latency=3
                *result=unsafe {core::mem::transmute(concat_idents!($begin,$maskadd)(core::mem::transmute_copy(result),mask,core::mem::transmute_copy(result),Self::AVX_ONE.$broadcast))}; // result+=a!=0, fast. latency=1
            }
            #[inline(always)]
            fn sub_ge(target: &mut [Self; $count], base: [Self; $count]) {
                use crate::simd::*;
                let mask=unsafe {concat_idents!($begin,$ge)(core::mem::transmute_copy(target),core::mem::transmute(base))}; // mask=a!=0, fast. latency=3
                *target=unsafe {core::mem::transmute(concat_idents!($begin,$masksub)(core::mem::transmute_copy(target),mask,core::mem::transmute_copy(target),core::mem::transmute(base)))}; // result+=a!=0, fast. latency=1
            }
        }
    }
}
