pub use crate::montgomery::*;
pub use crate::sugar::SugarMul;
pub use crate::vec_mul::*;

#[cfg(not(target_feature = "avx512ifma"))]
pub use crate::s32::*;
#[cfg(target_feature = "avx512ifma")]
pub use crate::s52::*;
