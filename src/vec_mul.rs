pub trait ModOps: Copy {
    type ModBase;
    #[inline(always)]
    fn add(&mut self, add: Self::ModBase);
    #[inline(always)]
    fn sgn(&self) -> Self::ModBase;
    fn sub_ge(&mut self, other: Self);
}
pub trait Mul1: Sized + Copy + ModOps {
    /// Multply 2 integer of this type together, the result is either `store`d or `add_assign`ed into `&mut reslo` and `reshi`, seperately. \
    /// This is the default implementation, call Mul1 twice, may slow down calculations.
    fn mul_assign(a: &[Self; 1], b: &[Self; 1], reslo: &mut [Self; 1], reshi: &mut [Self; 1]);
    /// Multply 2 integer of this type together, the result is either `store`d or `add_assign`ed into `&mut reslo` and `reshi`, seperately. \
    /// This is the default implementation, call Mul1 twice, may slow down calculations.
    fn mul(a: &[Self; 1], b: &[Self; 1], reslo: &mut [Self; 1], reshi: &mut [Self; 1]);
    /// SAFETY: res should be aligned properly, since it is already alighed, it is actually a safe function.
    unsafe fn mullo(a: &[Self; 1], b: &[Self; 1], res: [Self; 1]) -> [Self; 1];
    /// SAFETY: res should be aligned properly, since it is already alighed, it is actually a safe function.
    unsafe fn mulhi(a: &[Self; 1], b: &[Self; 1], res: [Self; 1]) -> [Self; 1];
    fn neq0p1(a: &[Self; 1], target: &mut [Self; 1]) {
        target[0].add(a[0].sgn())
    }
    #[inline(always)]
    fn sub_ge(target: &mut [Self; 1], base: [Self; 1]) {
        target[0].sub_ge(base[0])
    }
}
mul_impl!(
    (Mul2, Mul1, 1,doc="Multply 2 integer of this type together, the result is either `store`d or `add_assign`ed into `&mut reslo` and `reshi`, seperately.\nThis is the default implementation, call `Mul1` twice, may slow down calculations."),
    (Mul4, Mul2, 2,doc="Multply 4 integer of this type together, the result is either `store`d or `add_assign`ed into `&mut reslo` and `reshi`, seperately.\nThis is the default implementation, call `Mul2` twice, may slow down calculations."),
    (Mul8, Mul4, 4,doc="Multply 8 integer of this type together, the result is either `store`d or `add_assign`ed into `&mut reslo` and `reshi`, seperately.\nThis is the default implementation, call `Mul4` twice, may slow down calculations."),
    (Mul16, Mul8, 8,doc="Multply 16 integer of this type together, the result is either `store`d or `add_assign`ed into `&mut reslo` and `reshi`, seperately.\nThis is the default implementation, call `Mul8` twice, may slow down calculations.")
);
