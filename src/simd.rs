#[cfg(target_feature = "x86")]
pub(crate) use core::arch::x86::*;
#[cfg(not(target_feature = "x86"))]
pub(crate) use core::arch::x86_64::*;
#[derive(Copy, Clone)]
pub union Avx512 {
    pub m128: __m128i,
    pub m256: __m256i,
    pub m512: __m512i,
}
#[const_trait]
pub(crate) trait AvxInt: Sized + Copy {
    unsafe fn broadcast(self) -> __m512i;
}
mark_as_avx_int!(i8, i16, i32, i64, i128, u8, u16, u32, u64, u128, isize, usize);
impl Avx512 {
    pub(crate) const fn new<T: ~const AvxInt>(i: T) -> Self {
        unsafe {
            Self {
                m512: i.broadcast(),
            }
        }
    }
}

pub const AVX_ZERO: Avx512 = Avx512::new(0u128);
#[inline(always)]
pub(crate) const fn m128<T: Copy>(_: &[T]) -> crate::simd::__m128i {
    unsafe { AVX_ZERO.m128 }
}
#[inline(always)]
pub(crate) const fn m256<T: Copy>(_: &[T]) -> crate::simd::__m256i {
    unsafe { AVX_ZERO.m256 }
}
#[inline(always)]
pub(crate) const fn m512<T: Copy>(_: &[T]) -> crate::simd::__m512i {
    unsafe { AVX_ZERO.m512 }
}
