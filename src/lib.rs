#![allow(incomplete_features, unused_attributes)]
#![feature(
    allocator_api,
    concat_idents,
    const_trait_impl,
    doc_cfg,
    nonnull_slice_from_raw_parts,
    portable_simd,
    ptr_metadata,
    slice_as_chunks,
    specialization,
    stdsimd
)]
mod alloc;
#[macro_use]
mod macros;
pub mod montgomery;
pub mod prelude;
pub mod s32;
pub mod s52;
pub mod simd;
pub mod sugar;
pub mod vec_mul;
#[cfg(test)]
mod tests {
    use crate::prelude::*;
    #[test]
    fn mod_pow_test() {
        assert!(crate::s32::mod_pow(3, 35, 157) == 31);
        assert!(crate::s52::mod_pow(3, 35, 157) == 31);
    }
    #[test]
    #[cfg(target_feature = "avx512ifma")]
    fn mod52_test() {
        use crate::s52::ModExt;
        let test = ModExt::new(12347);
        assert!(test.rsq() == 7420);
        assert!(test.pinv() == 1485272348153613);
    }
    #[test]
    fn mod32_test() {
        use crate::s32::ModExt;
        let test = ModExt::new(12347);
        assert!(test.rsq() == 2451);
        assert!(test.pinv() == 3937720077);
    }
    #[test]
    fn rebase_test() {
        let test = ModExt::new(12323547);
        let mut test2 = ModExt::new(1232);
        test2.rebase(12323547);
        assert!(test.base() == test2.base());
        assert!(test.rsq() == test2.rsq());
        assert!(test.pinv() == test2.pinv());
    }
    #[cfg(target_feature = "avx512ifma")]
    #[test]
    fn add52_test() {
        let aa = core::simd::Simd::from([0i64; 8]).into();
        let a = unsafe {
            crate::simd::_mm512_madd52lo_epu64(
                aa, // result, input only.
                core::simd::Simd::from([1i64, 2, 3, 4, 5, 6, 7, 8]).into(),
                core::simd::Simd::from([1i64, 2, 3, 4, 5, 6, 7, 8]).into(),
            )
        };
        //dbg!{core::simd::Simd::<i64,8>::from(aa)};
        assert!(
            core::simd::Simd::from(a)
                == core::simd::Simd::<i64, 8>::from([1i64, 4, 9, 16, 25, 36, 49, 64])
        )
    }
    #[cfg(target_feature = "avx512ifma")]
    #[test]
    fn s52mul16_test() {
        let a = [ModBase(2u64.pow(26) + 1); 16];
        let b = a;
        let [mut c, mut d] = [[ModBase(1); 16]; 2];
        <ModBase as Mul16>::mul_assign(&a, &b, &mut c, &mut d);
        assert!(c == [ModBase(134217730); 16]);
        assert!(d == [ModBase(2); 16]);
        a.mul(&b, &mut c, &mut d); // sugar
        assert!(c == [ModBase(134217729); 16]);
        assert!(d == [ModBase(1); 16]);
    }
    #[cfg(target_feature = "avx512ifma")]
    #[test]
    fn mmul_test() {
        // SAFETY: Test Only.

        use crate::simd;
        #[rustfmt::skip]
        let mut a: [ModBase;8] = unsafe{core::mem::transmute([12326123u64, 534365, 1223657623412, 543657625, 4254364, 123266, 1225637, 67854])};
        let b = ModExt::new(1223657623469);
        use crate::montgomery::MMul8;
        unsafe {
            MMul8::mmul_inplace(
                &mut a,
                &core::mem::transmute(b.avx.2),
                &mut core::mem::transmute(simd::AVX_ZERO.m512),
                &mut core::mem::transmute(simd::AVX_ZERO.m512),
                b,
            );
        }
        assert_eq!(
            &a,
            &[
                ModBase(951940278013),
                ModBase(930087500708),
                ModBase(1059436949362),
                ModBase(10689336912),
                ModBase(1092428947079),
                ModBase(319516031190),
                ModBase(994293811207),
                ModBase(350558307424)
            ]
        );
        unsafe {
            MMul8::mmul_inplace(
                &mut a,
                &core::mem::transmute(ModBase::AVX_ONE.m512),
                &mut core::mem::transmute(simd::AVX_ZERO.m512),
                &mut core::mem::transmute(simd::AVX_ZERO.m512),
                b,
            );
        }
        #[rustfmt::skip]
        assert_eq!(a, unsafe{core::mem::transmute::<_,[ModBase;8]>([12326123u64, 534365, 1223657623412, 543657625, 4254364, 123266, 1225637, 67854])});
    }
}
