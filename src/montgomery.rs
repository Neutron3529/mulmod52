use crate::s52;
use crate::vec_mul::{Mul2, Mul4, Mul8};
mmul! {
    s52::ModBase, s52::ModExt, MMul2 ,Mul2 ,2, m128,0,1,2;
    s52::ModBase, s52::ModExt, MMul4 ,Mul4 ,4, m256,0,1,2;
    s52::ModBase, s52::ModExt, MMul8 ,Mul8 ,8, m512,0,1,2
}
